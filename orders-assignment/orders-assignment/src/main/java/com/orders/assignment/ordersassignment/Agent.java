package com.orders.assignment.ordersassignment;

import javax.persistence.*;

@Entity
@Table(name= "AGENTS", schema="[dbo]")
public class Agent {

    private String agent_code;
    private String agent_name;
    private String working_area;
    private double commission;
    private String phone_no;
    private String country;

    @Id
    @Column(name = "AGENT_CODE")
    public String getAgent_code() {
        return agent_code;
    }
    public void setAgent_code(String agent_code) { this.agent_code = agent_code; }

    @Basic
    @Column(name = "AGENT_NAME")
    public String getAgent_name() {
        return agent_name;
    }
    public void setAgent_name(String agent_name) { this.agent_name = agent_name; }

    @Basic
    @Column(name = "WORKING_AREA")
    public String getWorking_area() {
        return working_area;
    }
    public void setWorking_area(String working_area) { this.working_area = working_area; }

    @Basic
    @Column(name="COMMISSION")
    public double getCommission() {
        return commission;
    }

    public void setCommission(double comission) { this.commission = commission; }

    @Basic
    @Column(name="PHONE_NO")
    public String getPhone_no() { return phone_no;}
    public void setPhone_no(String phone_no) { this.phone_no = phone_no; }

    @Basic
    @Column(name="COUNTRY")
    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }
}
