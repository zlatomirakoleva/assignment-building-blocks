package com.orders.assignment.ordersassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersAssignmentApplication.class, args);
	}

}
