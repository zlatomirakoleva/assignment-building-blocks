package com.orders.assignment.ordersassignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {


	@Autowired
	private AgentRepository agentRepository;

	@RequestMapping("agents")
	public List<Agent> getAllAgents(){
		return agentRepository.findTop100By();
	}
	
	
}
